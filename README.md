RainbowTooth
============

Android Application for drawing an <a href="https://en.wikipedia.org/wiki/HLS_color_space">HSL</a> Rainbow.
Can connect to a Bluetooth device to send selected colours as data.

Dependancies
------------
* android-support-v7-appcompat.jar
* android-support-v4.jar
